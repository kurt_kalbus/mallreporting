package mallreporting;

import mallreporting.database.DBTenantInterface;
import mallreporting.database.DBUtils;
import java.lang.String;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import mallreporting.eurostop.Eurostop;
import mallreporting.lendlease.LendLease;
import mallreporting.queensbay.QueensBay;

/**
 *
 * @author kkalb
 */
public class MallReporting {
    
    public static String versionNumber = ".001";
    public static String appName = "mallreporting";
    public static MathContext mc = new MathContext(10,RoundingMode.HALF_UP);
   
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        ArrayList<TenantData> tenantDataRecords = null;
        
        try {
          
            DBUtils.initConnection();  
            DBUtils.initStatements();
            
            tenantDataRecords = DBTenantInterface.getTenantData();
            
            if (tenantDataRecords == null) {
                
                System.out.println ("Tenant data NOT FOUND!!");
                System.exit(1);
            }
            
            for (TenantData tenantData : tenantDataRecords)  {
                
                String interface_name = tenantData.get_interface_nm();
                
                if (interface_name.equals("eurostop")) {
                    
                    Eurostop eurostop = new Eurostop(tenantData);
                    eurostop.runIt();
                    
                } else if (interface_name.equals("lendlease")) {
                    
                    LendLease lendLease = new LendLease(tenantData);
                    lendLease.runIt();
                    
                } else if (interface_name.equals("queensbay")) {
                    
                    QueensBay queensBay = new QueensBay(tenantData);
                    queensBay.runIt();
                }
            }
           
        } catch (Exception ex) {
            
            System.out.println (ex.toString());
        }
    }
}

