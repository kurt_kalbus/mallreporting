/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mallreporting;

import java.math.BigDecimal;

/**
 *
 * @author kkalb
 */
public class TransHdrRecord {
    
    private String trans_hdr_id = "";
    private String customer_id = "";
    private String associate_id = "";
    private String trans_dt = "";
    private BigDecimal trans_amt = new BigDecimal("0.00", MallReporting.mc);
    private int trans_cnt = 0;
    private String trans_time = "";
    private String reference_txt = "";
    private String trans_type_cd = "";

    public TransHdrRecord (String trans_hdr_id, String date, int count,
                        String customer_id, String associate_id,
                        String trans_amt, String trans_time,
                        String trans_type_cd, String reference_txt) {
        
        this.trans_hdr_id = trans_hdr_id;
        this.trans_dt = date;
        this.customer_id = customer_id;
        this.associate_id = associate_id;
        this.trans_cnt = count;
        
        this.trans_amt =
                this.trans_amt.add(new BigDecimal (trans_amt, MallReporting.mc));
        
        this.trans_time = trans_time;
        this.trans_type_cd = trans_type_cd;
        this.reference_txt = reference_txt;
    }
    
    // Getters
    
    public String get_trans_hdr_id () {
        return trans_hdr_id;
    }
    
    public String get_customer_id () {
        return customer_id;
    }
    
    public String get_associate_id () {
        return associate_id;
    }
    
    public BigDecimal get_trans_amt () {
        return trans_amt;
    }
    
    public String get_trans_time () {
        return trans_time;
    }
    
    public String get_trans_type_cd () {
        return trans_type_cd;
    }
    
    public String get_trans_date () {
        return trans_dt;
    }
    
    public String get_reference_txt () {
        return reference_txt;
    }
}
