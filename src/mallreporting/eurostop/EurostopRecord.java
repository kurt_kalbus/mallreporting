/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mallreporting.eurostop;

import java.math.BigDecimal;

/**
 *
 * @author kkalb
 */
public class EurostopRecord {
    
    private String mall_code = "";
    private String tenant_code = "";
    private String till_no = "";
    private String date = "";
    private BigDecimal net_sales = new BigDecimal ("0.00", Eurostop.mc);
    
    public EurostopRecord (String mall_code, String tenant_code, String till_no,
                           String date, BigDecimal net_sales) {
                  
        this.mall_code = mall_code;
        this.tenant_code = tenant_code;
        this.till_no = till_no;
        this.date = date;
        this.net_sales = net_sales;
    }
    
    public String get_mall_code() {
        return mall_code;
    }
    
    public String get_tenant_code() {
        return tenant_code;
    }
    
    public String get_till_no() {
        return till_no;
    }
  
    public String get_date() {
        return date;
    }
    
    public BigDecimal get_net_sales() {
        
        return net_sales;
    }
}
