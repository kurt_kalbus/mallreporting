package mallreporting.eurostop;

import mallreporting.database.DBTransHdr;
import mallreporting.database.DBUtils;
import mallreporting.utils.JulianUtils;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.lang.String;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import mallreporting.*;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author kkalb
 */
public class Eurostop {
    
    public static String versionNumber = ".001";
    public static String appName = "eurostop";
    private static TenantData tenantData;
    private static String date = "";
    public static MathContext mc = new MathContext(10,RoundingMode.HALF_UP);
   
    private static SimpleDateFormat formatOld = new SimpleDateFormat("dd-MMM-yyyy");
    private static SimpleDateFormat formatYMD = new SimpleDateFormat("yyyyMMdd");
    private static SimpleDateFormat formatReport = new SimpleDateFormat("yyyy-MM-dd");
             
    private static ArrayList<EurostopRecord> records = new ArrayList<EurostopRecord>();
    
    private static String ftp_host = "";
    private static String ftp_user = "";
    private static String ftp_enc_password = "";
           
    /**
     * @param args the command line arguments
     */
      
    public Eurostop (TenantData tenantData) {
        
        this.tenantData = tenantData;
    }
        
    public void runIt() {

        try {
            
            DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy");
            Calendar today = Calendar.getInstance();
            date = dateFormat.format(today.getTime());
            
            DBUtils.initConnection();  
            DBUtils.initStatements();
           
            ftp_host = tenantData.get_ftp_host();
            ftp_enc_password = tenantData.get_ftp_enc_password();
            ftp_user = tenantData.get_ftp_user();
            
            // Get the report Julian date so we can easily count backward
            // count backward 30 days
            
            Date reportDateOld = formatOld.parse(date);
            String ymdString = formatYMD.format(reportDateOld);
            String reportDateNew = formatReport.format(reportDateOld);
             
            int[] ymd = new int[3];
            ymd[0] = Integer.valueOf(ymdString.substring(0,4));
            ymd[1] = Integer.valueOf(ymdString.substring(4,6));
            ymd[2] = Integer.valueOf(ymdString.substring(6,8));
            
            double reportJulianDate = JulianUtils.toJulian(ymd);
       
            ArrayList<EurostopRecord> records = new ArrayList<EurostopRecord>();
             
            for (int i=0; i<30; i++) {
                 
                double julianDate = reportJulianDate-i;
                 
                // Convert julian date back into a format that the 
                // database understands
                 
                ymd = JulianUtils.fromJulian(julianDate);
                
                String yearS = String.valueOf(ymd[0]);
                String monthS = String.valueOf(ymd[1]);
                String dayS = String.valueOf(ymd[2]);
                 
                // The parsing format wants 2 characters for month and day
                
                if (ymd[1] < 10) {
                    monthS = " "+monthS;
                }
                
                if (ymd[2] < 10) {
                    dayS = " "+dayS;
                }
                
                ymdString = yearS + monthS + dayS;
               
                Date day = formatYMD.parse(ymdString);
                String dayString = formatOld.format(day);
                 
                ArrayList<TransHdrRecord> transactions =
                    DBTransHdr.findTransactionsByDate (dayString);
             
                BigDecimal net_sales = new BigDecimal("0.00", Eurostop.mc);

                for (TransHdrRecord trans : transactions) {

                    net_sales = net_sales.add(trans.get_trans_amt());
                }
                
                String reportDayString = formatReport.format(day) + "T12:00:00";
                
                EurostopRecord record =
                        new EurostopRecord(tenantData.get_mall_id(),
                                           tenantData.get_store_id(),
                                           tenantData.get_pos_id(),
                                           reportDayString, net_sales);
                
                records.add(record);
            }
           
            String filename  = tenantData.get_mall_id()  +
                               tenantData.get_store_id() +
                               reportDateNew.substring(2,4) +
                               reportDateNew.substring(5,7) +
                               reportDateNew.substring(8,10) +
                               "120000.csv";
             
            System.out.println (" fileName = "+filename);
             
            String delim = ",";
             
            ArrayList<String> reportLines = new ArrayList<String>();
             
            for (EurostopRecord record: records) {
                 
                String line = record.get_mall_code() + delim +
                              record.get_tenant_code() + delim +
                              record.get_till_no() + delim +
                              record.get_date() + delim +
                              record.get_net_sales().toString();
                
                reportLines.add(line);
           }
                        
            System.out.println ("writing file");
            WriteReportToFile.write (reportLines, filename);
  
            FTPClient ftp = new FTPClient();
            ftp.connect(ftp_host);
           
            String dec_password = new String(Base64New.decode(ftp_enc_password));
            
            if (ftp.login(ftp_user, dec_password)) {
                System.out.println ("FTP Connected!");
            } else {
                System.out.println ("FTP Failed!");
                System.exit(ftp.getReplyCode());
            }
            
            System.out.println ("reply code = "+ftp.getReplyCode());
            System.out.println ("reply string = "+ftp.getReplyString());
          
            BufferedInputStream bos = new BufferedInputStream(new FileInputStream (filename));
            if (ftp.storeFile(filename, bos)) {
                
                System.out.println ("ftp worked");
            } else {
                System.out.println ("ftp sucked");
                System.exit(ftp.getReplyCode());
            }
            
            int retCode = ftp.getReplyCode();
            System.out.println ("reply code = "+retCode);
            System.out.println ("reply string = "+ftp.getReplyString());
            bos.close();
            
            ftp.disconnect();
            
        } catch (Exception ex) {
            
            System.out.println (ex.toString());
        }
    }
    
    public static void main() {
        
        Eurostop euroStop = new Eurostop(null);
        euroStop.runIt(); 
    }
}

