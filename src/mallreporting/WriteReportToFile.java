/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mallreporting;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 *
 * @author kkalb
 */
public class WriteReportToFile {
    
    public static void write (ArrayList<String> lines, String fileName) {
        write (lines, null, fileName);
    }
    
    public static void write(ArrayList<String> lines, ArrayList<String> columnHeadings, String fileName) {
        
        PrintWriter out = null;
        File file = new File(fileName);   
        if (file.exists()) {
            file.delete();
        }
        
        file = new File(fileName);
        
        try {
            out = new PrintWriter(new FileWriter(file));   
        } catch (IOException e){}
        
        if (out == null) {
            return;
        }
  
        int count = 0;
        
        // Write each string in the array on a separate line   
        for (String s : lines) {   
            out.println(s);   
            count++;
            
            if (count == 4 && columnHeadings != null) {
            
                for (String heading : columnHeadings) {   
                    out.println(heading);   
                }
            }
        }   
  
        out.close();  
    }
}
