/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mallreporting;

/**
 *
 * @author kkalb
 */
public class TenantData {
    
    private String interface_nm;
    private String mall_id = "";
    private String store_id = "";
    private String pos_id = "";
    private int batch_id = 0;
    private String ftp_url = "";
    private String update_dt;
    private String ftp_user = "";
    private String ftp_enc_password = "";
    private String ftp_host = "";

    public TenantData (String interface_nm, String mall_id, String store_id,
                       String pos_id, int batch_id, String ftp_url,
                       String update_dt) {
                  
        this.interface_nm = interface_nm;
        this.mall_id = mall_id;
        this.store_id = store_id;
        this.pos_id = pos_id;
        this.batch_id = batch_id;
        this.ftp_url = ftp_url;
        this.update_dt = update_dt;
        ftp_host = ftp_url;
        
        if (ftp_url.startsWith("ftp")) {
            String tempString = ftp_url.substring(6);
            int idx = tempString.indexOf(":");
            
            if (idx > 0) {
                ftp_user = tempString.substring(0, idx);
                int idx2 = tempString.indexOf("@");
                ftp_host = tempString.substring(idx2+1);
                ftp_enc_password = tempString.substring (idx+1, idx2);
                System.out.println ("enc_password = "+ftp_enc_password);
            }
        }
    }
    
    public TenantData() {
        
    }
    
    // Getters
    
    public String get_ftp_user () {
        return ftp_user;
    }
      
    public String get_ftp_enc_password () {
        return ftp_enc_password;
    }
    
    public String get_ftp_host () {
        return ftp_host;
    }
    
    public String get_mall_id () {
        return mall_id;
    }
    
    public String get_store_id () {
        return store_id;
    }
    
    public String get_pos_id () {
        return pos_id;
    }
    
    public int get_batch_id () {
        return batch_id;
    }
    
    public String get_ftp_url () {
        return ftp_url;
    }
    
    public String get_update_dt () {
        return update_dt;
    }
    
    public String get_interface_nm () {
        return interface_nm;
    }
}
