package mallreporting.database;

import mallreporting.MallReporting;
import java.sql.*;

/**
 *
 * @author kkalb
 */
public class DBUtils {
    
    private static Connection conn;
    public static String databaseProductVersion1 = "";
    public static String databaseProductVersion2 = "";
    public static String jdbcDriverVersion = "";
    
    private static String credentials1 = "jdbc:oracle:thin:pos/welcome1@localhost:1521:xe";
    private static String credentials2 = "jdbc:oracle:thin:pos/welcome1@xdevb.sunrider.com:1521:xdevbxe";
    
    // This is ONLY used for putting phoney transactions in the Database
    //public static String phoney_date = null;
    
    private static ResultSet rs;
    
    public static void initConnection() throws Exception {
  
        // Try the localhost connection first. If that doesn't work, it's
        // probably me running this, so try the xdevb connection. This way
        // I don't have to keep changing the connection string when I build
        // the jar file for the testers.
        
        try {
            
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
            conn = DriverManager.getConnection (credentials1);
        } catch (java.sql.SQLException ex1) {
            
            System.out.println (" trying alternate connection");
            try {
            
                DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
                conn = DriverManager.getConnection (credentials2);
            } catch (java.sql.SQLException ex2) {
                System.out.println("Failed to open connection: " + ex2.getMessage());
                throw new Exception("Failed to open connection. ");
            }
        }
    }

    public static String testConnection() {
        
        String returnString = "";
        
        try {
            DatabaseMetaData dmd = conn.getMetaData();
            databaseProductVersion1 = dmd.getDatabaseProductVersion();
            int index = databaseProductVersion1.indexOf("Release");
            databaseProductVersion2 = databaseProductVersion1.substring(index);
            databaseProductVersion1 = databaseProductVersion1.substring(0,index);
            
            jdbcDriverVersion = dmd.getDriverVersion();
            
            returnString = "Sunrider Mall Reporting System Version "+MallReporting.versionNumber+"\n"
                + dmd.getDatabaseProductVersion() + "\n"
                + "JDBC Driver Version " + dmd.getDriverVersion() + "\n"
                + "URL: " + dmd.getURL() + "\n";
            
        } catch (java.sql.SQLException ex) {
            
           returnString = "ERROR: Could not connect to internal database!\n)";
        }
        
        return returnString;
    }
    
    public static void initStatements() {
     
        DBTransHdr.init();
    }
  
    public static Connection getConnection() {
        return conn;
    }
}
