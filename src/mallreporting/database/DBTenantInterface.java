package mallreporting.database;

import mallreporting.Base64New;
import mallreporting.TenantData;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

/**
 *
 * @author kkalb
 */
public class DBTenantInterface {

    private static PreparedStatement insertData;
    private static ResultSet rs;
    
    public static TenantData getTenantData (String interface_nm) {

        TenantData tenantData = null;
        
         try {
             
             PreparedStatement getTenantData = DBUtils.getConnection().prepareStatement (
                  "select mall_id, store_id, pos_id, batch_id, ftp_url, "
                + "to_char(update_dt, 'DD-MON-YYYY hh24:mi') as time from tenant_interface "
                + "where interface_nm='" +interface_nm+ "'");
             
            rs = getTenantData.executeQuery();
            
            while (rs.next()) {
                String mall_id = rs.getString("mall_id");
                String store_id = rs.getString("store_id");
                String pos_id = rs.getString("pos_id");
                int batch_id = rs.getInt("batch_id");
                String ftp_url = rs.getString("ftp_url");
                String update_dt = rs.getString("time");
                
                tenantData = new TenantData (interface_nm, mall_id, store_id,
                                            pos_id, batch_id, ftp_url, update_dt);
            }
            
        } catch (java.sql.SQLException ex) {
            
            System.out.println("DBTenantDate.init Database Error! Please see console log. "+ex.toString());   
        }
         
        return tenantData;
    }
  
    public static ArrayList<TenantData> getTenantData () {

        ArrayList<TenantData> tenantData = new ArrayList<TenantData>();
        
         try {
             
             PreparedStatement getTenantData = DBUtils.getConnection().prepareStatement (
                  "select interface_nm, mall_id, store_id, pos_id, batch_id, ftp_url, "
                + "to_char(update_dt, 'DD-MON-YYYY hh24:mi') as time from tenant_interface");
             
            rs = getTenantData.executeQuery();
            
            while (rs.next()) {
                String interface_nm = rs.getString("interface_nm");
                String mall_id = rs.getString("mall_id");
                String store_id = rs.getString("store_id");
                String pos_id = rs.getString("pos_id");
                int batch_id = rs.getInt("batch_id");
                String ftp_url = rs.getString("ftp_url");
                String update_dt = rs.getString("time");
                
                tenantData.add(
                        new TenantData (interface_nm, mall_id, store_id, pos_id,
                                        batch_id, ftp_url, update_dt));
            }
            
        } catch (java.sql.SQLException ex) {
            
            System.out.println("getTenantData Database Error! Please see console log. "+ex.toString());      
        }
         
        return tenantData;
    }
    
    public static boolean updateData (String interface_nm, String mall_id,
                                   String store_id, String pos_id,
                                   int batch_id, String ftp_host, String ftp_user,
                                   String ftp_password) {
        
        String timeStamp = "";
        boolean success = true;
        String updateSQL = "";
        
        String enc_password =  Base64New.encode(ftp_password.getBytes());
        
        System.out.println ("enc+password = "+enc_password);
        
        String ftp_url = "ftp://" +ftp_user+ ":" +enc_password+ "@" +ftp_host;
     
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        Calendar today = Calendar.getInstance();
        timeStamp = dateFormat.format(today.getTime());
        
        TenantData data = getTenantData(interface_nm);
        
        String orig_ftp_url = data.get_ftp_url();
        int idx1 = orig_ftp_url.indexOf(":");
        int idx2 = orig_ftp_url.indexOf("@");
        //String enc_password
        
        if (ftp_password.equals("")) {
            
            updateSQL = "update tenant_interface set "
                    + "mall_id = '" +mall_id+ "', "
                    + "store_id ='"  +store_id+ "', "
                    + "pos_id = '"+pos_id+"', "
                    + "batch_id = "+ String.valueOf(batch_id) +", "
                    + "ftp_url = '" +ftp_url+ "', "
                    + "update_dt = to_date('" +timeStamp+ "', 'DD-MON-YYYY HH24:MI') "
                    + "where interface_nm = '"+interface_nm+"'";
        } else {
            
            updateSQL = "update tenant_interface set "
                    + "mall_id = '" +mall_id+ "', "
                    + "store_id ='"  +store_id+ "', "
                    + "pos_id = '"+pos_id+"', "
                    + "batch_id = "+ String.valueOf(batch_id) +", "
                    + "ftp_url = '" +ftp_url+ "', "
                    + "update_dt = to_date('" +timeStamp+ "', 'DD-MON-YYYY HH24:MI') "
                    + "where interface_nm = '"+interface_nm+"'";
        }
        
        System.out.println (" updateSQL + "+updateSQL);
        
        try {   
            PreparedStatement updateData =
                    DBUtils.getConnection().prepareStatement (updateSQL); 
        
            int cnt = updateData.executeUpdate();
            updateData.close();
            System.out.println (" update tenant update count = "+cnt);
            
            if (cnt == 0) {
                
                // Update failed - there is no record for this item for the given
                // month, so create one.
                
                String insertSQL = "insert into tenant_interface "
                                + "(interface_nm, mall_id, store_id, pos_id, "
                                + "batch_id, ftp_url, update_dt) "
                                + "values(?, ?, ?, ?, ?, ?, ?)";
                
                insertData = DBUtils.getConnection().prepareStatement (insertSQL); 
                
                insertData.setString(1,interface_nm);
                insertData.setString(2,mall_id);
                insertData.setString(3,store_id);
                insertData.setString(4,pos_id);
                insertData.setInt(5,batch_id);
                insertData.setString(6,ftp_url);
                
                insertData.setString(7, "to_date('" +timeStamp+ "', 'DD-MON-YYYY HH24:MI')");
                
                cnt = insertData.executeUpdate();
                insertData.close();
                System.out.println (" item_activity insert count = "+cnt);
            }
                
        }  catch (java.sql.SQLException ex) {
            
            System.out.println("updateData Database Error! Please see console log. "+ex.toString());
            success = false;
        }
        
        updateUpdateDate();
        return success;
    }
    
    public static boolean deleteInterface (String interface_nm) {
        
        boolean success = true;
        
        String deleteSQL = "delete from tenant_interface where interface_nm='"
                + interface_nm +"'";
        
        try {   
            PreparedStatement deleteData =
                    DBUtils.getConnection().prepareStatement (deleteSQL); 
        
            int cnt = deleteData.executeUpdate();
            deleteData.close();
            System.out.println (" update tenant update count = "+cnt);
            
        }  catch (java.sql.SQLException ex) {
            
            System.out.println("deleteInterface Database Error! Please see console log. "+ex.toString());
            success = false;
        }
        
        updateUpdateDate();
        return success;
    }
    
    public static boolean insertData (String interface_nm, String mall_id,
                                      String store_id, String pos_id,
                                      int batch_id, String ftp_host,
                                      String ftp_user, String ftp_password) {
        boolean success = true;
        
        String enc_password =  Base64New.encode(ftp_password.getBytes());
        
        String ftp_url = "ftp://" +ftp_user+ ":" +enc_password+ "@" +ftp_host;
     
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
        Calendar today = Calendar.getInstance();
        
        String insertSQL = "insert into tenant_interface "
                + "(interface_nm, mall_id, store_id, pos_id, batch_id, ftp_url) "
                + "values('"  +interface_nm+ "','"+mall_id+ "','" 
                +  store_id+ "','" +pos_id+ "','" +batch_id+ "','" +ftp_url+ "')";
        
        System.out.println (" insertSQL = "+ insertSQL);
          
        try {   
          
            insertData = DBUtils.getConnection().prepareStatement (insertSQL); 
            
            int cnt = insertData.executeUpdate();
            insertData.close();
            System.out.println (" tenant data insert count = "+cnt);
              
        }  catch (java.sql.SQLException ex) {
            
            System.out.println(" Database Error! Please see console log. "+ex.toString());
            success = false;
        }
        
        updateUpdateDate();
        return success;
    }
    
    public static void updateUpdateDate() {
        
        try {
            DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm");
            Calendar today = Calendar.getInstance();
            String timeStamp = dateFormat.format(today.getTime());

            String updateSQL = "update tenant_interface set "
                    + "update_dt = to_date('" +timeStamp+ "', 'DD-MON-YYYY HH24:MI') ";

            PreparedStatement updateData =
                    DBUtils.getConnection().prepareStatement (updateSQL); 

            int cnt = updateData.executeUpdate();
            updateData.close();
        
        } catch (java.sql.SQLException ex) {
            System.out.println("updateUpdateDate Error update date admin table: " + ex.getMessage());
        }
    }
}
