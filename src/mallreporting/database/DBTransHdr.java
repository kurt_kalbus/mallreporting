package mallreporting.database;

import mallreporting.TransHdrRecord;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author kkalb
 */
public class DBTransHdr {

    private static PreparedStatement findTransactionsByDate;
    private static PreparedStatement findTransactionsByDateOrderByTime;
    private static PreparedStatement findTransactionsByDateOrderByTransId;
    private static PreparedStatement findSalesByDateOrderByTime;
    private static PreparedStatement findTransactionsByDateAndAssociate;
    private static PreparedStatement findTransactionsByMemberIdPerMonth;
    private static PreparedStatement findSaleHdrIdByMonth;
    private static PreparedStatement findInvenReceiptsByMonth;
    private static PreparedStatement findTransactionDateBySaleHdrId;
    private static PreparedStatement updateTransCnt;
    private static PreparedStatement findTransactionsByMonth;
    private static PreparedStatement findTransactionsByRange;
    private static PreparedStatement findTransactionsByMonthAndAssociate;
    private static ResultSet rs;
    
    private static String sql10 = "update trans_hdr set trans_cnt =  ? "
                + "where trans_hdr_id = ?";
    
    private static String sql20 = "select trans_hdr_id, trans_dt, trans_type_cd, "
                + "customer_id, reference_txt from trans_hdr where " 
                + "trans_dt >= to_date(?,'DD-MON-YYYY:hh24:mi') and "
                + "trans_dt <= to_date(?,'DD-MON-YYYY:hh24:mi')  order by trans_hdr_id";
        
    public static void init() {
      
         String sql1 = "select trans_hdr_id, customer_id, associate_id, trans_amt, trans_dt, trans_cnt, "
                + "to_char(trans_dt, 'DD-MON-YYYY hh24:mi') as time "
                + "from trans_hdr where trans_dt >= to_date(?,'DD-MON-YYYY:hh24:mi') "
                + "and trans_dt <= to_date(?,'DD-MON-YYYY:hh24:mi')";
        
         String sql2 = "select trans_hdr_id, trans_amt, trans_cnt,"
                + "to_char(trans_dt, 'DD-MON-YYYY hh24:mi') as time "
                + "from trans_hdr where trans_dt >= to_date(?,'DD-MON-YYYY:hh24:mi') "
                + "and trans_dt <= to_date(?,'DD-MON-YYYY:hh24:mi') and associate_id = ? "
                + " order by time";
         
         System.out.println ("sql2 = "+sql2);
         
         String sql3 = "select trans_hdr_id, customer_id, associate_id, trans_amt, trans_dt, trans_cnt, trans_type_cd, "
                + "to_char(trans_dt, 'DD-MON-YYYY hh24:mi') as time "
                + "from trans_hdr where trans_dt >= to_date(?,'DD-MON-YYYY:hh24:mi') "
                + "and trans_dt <= to_date(?,'DD-MON-YYYY:hh24:mi') order by trans_dt";
         
         String sql3a = "select trans_hdr_id, customer_id, associate_id, trans_amt, trans_dt, trans_cnt, "
                + "to_char(trans_dt, 'DD-MON-YYYY hh24:mi') as time "
                + "from trans_hdr where trans_dt >= to_date(?,'DD-MON-YYYY:hh24:mi') "
                + "and trans_dt <= to_date(?,'DD-MON-YYYY:hh24:mi')  and trans_type_cd = 'S' order by trans_dt";
         
         String sql3b = "select trans_hdr_id, customer_id, associate_id, trans_amt, trans_dt, trans_cnt, trans_type_cd, "
                + "to_char(trans_dt, 'DD-MON-YYYY hh24:mi') as time "
                + "from trans_hdr where trans_dt >= to_date(?,'DD-MON-YYYY:hh24:mi') "
                + "and trans_dt <= to_date(?,'DD-MON-YYYY:hh24:mi') order by trans_hdr_id";
       
        String sql4 = "select trans_hdr_id, trans_amt, trans_cnt, " 
                +"to_char(trans_dt, 'DD-Mon-YYYY') as saledate from trans_hdr "
                +"where to_char (trans_dt, 'Mon-YYYY') = ? and customer_id = ? order by saledate";
        
        String sql5 = "select trans_hdr_id from trans_hdr where to_char (trans_dt, 'Mon-YYYY') = ? ";
        
        String sql6 = "select trans_dt from trans_hdr where trans_hdr_id = ? ";
        
        String sql7 = "select trans_hdr_id, reference_txt, trans_dt from trans_hdr "
                + "where to_char (trans_dt, 'Mon-YYYY') = ? and trans_type_cd = 'I' "
                + "order by reference_txt";
        
        String sql8 = "select trans_hdr_id, trans_amt, trans_cnt, trans_type_cd, associate_id, " 
                + "to_char(trans_dt, 'DD-Mon-YYYY') as saledate from trans_hdr "
                + "where to_char (trans_dt, 'Mon-YYYY') = ? order by saledate";
        
        String sql9 = "select trans_hdr_id, trans_amt, trans_cnt, trans_type_cd, associate_id, " 
                + "to_char(trans_dt, 'DD-Mon-YYYY') as saledate from trans_hdr "
                + "where to_char (trans_dt, 'Mon-YYYY') = ? and associate_id = ? "
                + "order by saledate";
               
        try {
            findTransactionsByDate = DBUtils.getConnection().prepareStatement (sql1);
            findTransactionsByDateAndAssociate = DBUtils.getConnection().prepareStatement (sql2);
            findTransactionsByDateOrderByTime = DBUtils.getConnection().prepareStatement (sql3);
            findSalesByDateOrderByTime = DBUtils.getConnection().prepareStatement (sql3a);
            findTransactionsByDateOrderByTransId = DBUtils.getConnection().prepareStatement (sql3b);
            findTransactionsByMemberIdPerMonth = DBUtils.getConnection().prepareStatement (sql4);
            findSaleHdrIdByMonth = DBUtils.getConnection().prepareStatement (sql5);
            findTransactionDateBySaleHdrId = DBUtils.getConnection().prepareStatement (sql6);
            findInvenReceiptsByMonth = DBUtils.getConnection().prepareStatement (sql7);
            findTransactionsByMonth = DBUtils.getConnection().prepareStatement (sql8);
            findTransactionsByMonthAndAssociate = DBUtils.getConnection().prepareStatement (sql9);
            findTransactionsByRange = DBUtils.getConnection().prepareStatement(sql20);
               
        } catch (java.sql.SQLException ex) {
            System.out.println("Preparing select statement: " + ex.getMessage());
        }
    }
    
    public static void voidTransHdrRecord (String trans_hdr_id,
                                           String assoc_id,
                                           String reason) {
        
        String sql = "update trans_hdr set trans_cnt=0, trans_amt='0.00', "
                   + "trans_type_cd ='V', associate_id = '" +assoc_id 
                   + "', reference_txt = '" +reason+ "', upload_dt = NULL"
                   + " where trans_hdr_id=" +trans_hdr_id;
        
        try {
            Statement ss = DBUtils.getConnection().createStatement();
            
            System.out.println("trans_hdr void: "+sql);
            int rowCount = ss.executeUpdate(sql);
            System.out.println("trans_hdr count = " + rowCount);
            ss.close();

        } catch (java.sql.SQLException ex) {
            
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
    }
    
    public static String getCustIDFromTransHdrId (String trans_hdr_id) {
        
        String query = "select customer_id from trans_hdr where trans_hdr_id = '"
                     + trans_hdr_id+ "'";
        
        String custId = null;
        
         try {
            Statement ss = DBUtils.getConnection().createStatement();
           
            ResultSet result = ss.executeQuery(query);
            
            if (result.next()) {
                custId = result.getString("customer_id");
                System.out.println("customer_id = " + custId);
            }
            result.close();
          
         } catch (java.sql.SQLException ex) {
            
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }

         return custId;
    }
 
    public static int getMaxTransHdrId() {
        
        int maxId = 0;
        
        String query = "select max(trans_hdr_id) from trans_hdr";
       
        try {
            Statement ss = DBUtils.getConnection().createStatement();
            
            ResultSet result = ss.executeQuery(query);
            if(result.next()) {

                maxId = result.getInt(1);
            }
            result.close();
           
        } catch (java.sql.SQLException ex) {
            
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
         
        return maxId;  
    }
 
    public static String getLastSaleReceiptNumber() {
        
        String query = "select max(trans_hdr_id) from trans_hdr where trans_type_cd='S'";
        String receiptNumber = "";;
        
         try {
            Statement ss = DBUtils.getConnection().createStatement();
            
            ResultSet result = ss.executeQuery(query);
            if(result.next()) {

                receiptNumber = result.getString(1);
            }
            result.close();
           
         } catch (java.sql.SQLException ex) {
            
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
         
         return receiptNumber;
    }
    
    public static void updateTransCnt (int trans_cnt, String trans_hdr_id) {
        
        try {
            updateTransCnt = DBUtils.getConnection().prepareStatement (sql10);
            
            updateTransCnt.setInt(1, trans_cnt);
            updateTransCnt.setString(2, trans_hdr_id);
            
            int num = updateTransCnt.executeUpdate();
            
            System.out.println (" updated " + num + " trans_cnt");
            updateTransCnt.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred updating trans_hdr: " + ex.getMessage());
        }
    }
    
    /*
     * 
     */
    public static ArrayList<TransHdrRecord> findInventoryReceiptsByMonth (String month) {
       
        ArrayList<TransHdrRecord> transHdrRecords = new ArrayList<TransHdrRecord>();
       
        try {

            findInvenReceiptsByMonth.setString(1,month);
            rs = findInvenReceiptsByMonth.executeQuery();
         
            while (rs.next()) {

                String trans_hdr_id = rs.getString("trans_hdr_id");
                String reference_txt = rs.getString("reference_txt");
                String trans_dt = rs.getString("trans_dt");
                
                TransHdrRecord thr = new TransHdrRecord(trans_hdr_id, trans_dt, 0,
                                                        null, null, "0.00", null, "",
                                                        reference_txt);
                        
                transHdrRecords.add (thr);
                
            }
            
            rs.close();
          
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
        return transHdrRecords;
    }
    
    /*
     * 
     */
    public static ArrayList<String> findSaleHdrIdsByMonth (String month) {
       
        ArrayList<String> saleHdrIds = new ArrayList<String>();
        try {

            findSaleHdrIdByMonth.setString(1,month);
            rs = findSaleHdrIdByMonth.executeQuery();
         
            while (rs.next()) {

                saleHdrIds.add(rs.getString("trans_hdr_id"));
            }
            
            rs.close();
          
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
        return saleHdrIds;
    }
    
    /*
     * 
     */
    public static ArrayList<TransHdrRecord> findTransactionsByDate (String date) {
       
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
        try {
            
            String sql666 = "select trans_hdr_id, customer_id, associate_id, trans_amt, trans_dt, trans_cnt, "
                + "to_char(trans_dt, 'DD-MON-YYYY hh24:mi') as time "
                + "from trans_hdr where trans_dt >= to_date('" +date+ ":00:00'" + ",'DD-MON-YYYY:hh24:mi') "
                + "and trans_dt <= to_date('" +date+ ":23:59'" + ",'DD-MON-YYYY:hh24:mi')";
            
            PreparedStatement findTransactionsByDate1 = DBUtils.getConnection().prepareStatement (sql666);
            
             rs = findTransactionsByDate1.executeQuery();

            System.out.println ("sql666="+sql666);
           System.out.println ("date = "+date);
            
           // findTransactionsByDate.setString(1,date+":00:00");
          //  findTransactionsByDate.setString(2,date+":23:59");
           // rs = findTransactionsByDate.executeQuery();
         
            while (rs.next()) {

                String trans_hdr_id = rs.getString("trans_hdr_id");
                String customer_id = rs.getString("customer_id");
                String associate_id = rs.getString("associate_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_dt = rs.getString("trans_dt");
                int trans_cnt = rs.getInt("trans_cnt");
                String trans_time = rs.getString("time").substring(12);
                
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, trans_dt, trans_cnt, customer_id,
                                           associate_id, trans_amt, trans_time, "", null);
                
                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
        return transactions;
    }
    
    public static ArrayList<TransHdrRecord>
            findTransactionsByRange (String fromDate, String toDate) {
       
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
        try {

            System.out.println ("from to dates : "+ fromDate +" "+ toDate);
            System.out.println ("sql20 = "+sql20);
            findTransactionsByRange.setString(1,fromDate+":00:00");
            findTransactionsByRange.setString(2,toDate+":23:59");
            rs = findTransactionsByRange.executeQuery();
         
            while (rs.next()) {

                String trans_hdr_id = rs.getString("trans_hdr_id");
                String trans_dt = rs.getString("trans_dt");
                String trans_type_cd = rs.getString("trans_type_cd");
                String customer_id = rs.getString("customer_id");
                String reference_txt = rs.getString("reference_txt");
                
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, trans_dt,
                                            0, customer_id, "", "0.00", "", 
                                            trans_type_cd, reference_txt);
                
                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
        return transactions;
    }
    
    public static ArrayList<TransHdrRecord> findTransactionsByDateOrderByTime (String date) {
       
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
        try {

            findTransactionsByDateOrderByTime.setString(1,date+":00:00");
            findTransactionsByDateOrderByTime.setString(2,date+":23:59");
            rs = findTransactionsByDateOrderByTime.executeQuery();
           
            while (rs.next()) {
                
                String trans_hdr_id = rs.getString("trans_hdr_id");
                String customer_id = rs.getString("customer_id");
                String associate_id = rs.getString("associate_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_dt = rs.getString("trans_dt");
                String trans_type_cd = rs.getString("trans_type_cd");
                int trans_cnt = rs.getInt("trans_cnt");
                String trans_time = rs.getString("time").substring(12);
                
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, trans_dt, trans_cnt, customer_id,
                                           associate_id, trans_amt, trans_time, trans_type_cd, null);

                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
        return transactions;
    }
    
    public static ArrayList<TransHdrRecord> findTransactionsByDateOrderByTransId (String date) {
       
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
        try {

            findTransactionsByDateOrderByTransId.setString(1,date+":00:00");
            findTransactionsByDateOrderByTransId.setString(2,date+":23:59");
            rs = findTransactionsByDateOrderByTransId.executeQuery();
           
            while (rs.next()) {
                
                String trans_hdr_id = rs.getString("trans_hdr_id");
                String customer_id = rs.getString("customer_id");
                String associate_id = rs.getString("associate_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_dt = rs.getString("trans_dt");
                String trans_type_cd = rs.getString("trans_type_cd");
                int trans_cnt = rs.getInt("trans_cnt");
                String trans_time = rs.getString("time").substring(12);
                
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, trans_dt, trans_cnt, customer_id,
                                           associate_id, trans_amt, trans_time, trans_type_cd, null);

                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
        return transactions;
    }
    
    public static ArrayList<TransHdrRecord> findSalesByDateOrderByTime (String date) {
       
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
        try {

            findSalesByDateOrderByTime.setString(1,date+":00:00");
            findSalesByDateOrderByTime.setString(2,date+":23:59");
            rs = findSalesByDateOrderByTime.executeQuery();
           
            while (rs.next()) {
                
                String trans_hdr_id = rs.getString("trans_hdr_id");
                String customer_id = rs.getString("customer_id");
                String associate_id = rs.getString("associate_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_dt = rs.getString("trans_dt");
                int trans_cnt = rs.getInt("trans_cnt");
                String trans_time = rs.getString("time").substring(12);
                
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, trans_dt, trans_cnt, customer_id,
                                           associate_id, trans_amt, trans_time, "", null);

                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
        return transactions;
    }
    
    /*
     * 
     */
    public static ArrayList<TransHdrRecord>
            findTransactionsByDateAndAssociate (String associate_id, String date) {
       
        ArrayList<TransHdrRecord> transactions =
                new ArrayList<TransHdrRecord>();
        
        try {

            findTransactionsByDateAndAssociate.setString(1,date+":00:00");
            findTransactionsByDateAndAssociate.setString(2,date+":23:59");
            findTransactionsByDateAndAssociate.setString(3,associate_id);
            rs = findTransactionsByDateAndAssociate.executeQuery();
           
            while (rs.next()) {

                String trans_hdr_id = rs.getString("trans_hdr_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_time = rs.getString("time").substring(12);
                int trans_cnt = rs.getInt("trans_cnt");
               
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, null, trans_cnt, null,
                                           null, trans_amt, trans_time, "", null);
                
                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
        return transactions;
    }
    
    /*
     * 
     */
    public static ArrayList<TransHdrRecord> findTransactionsByMonth (String date) {
            
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
                
        try {

            findTransactionsByMonth.setString(1,date);
            rs = findTransactionsByMonth.executeQuery();
           
            while (rs.next()) {

                String trans_hdr_id = rs.getString("trans_hdr_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_date = rs.getString("saledate");
                int trans_cnt = rs.getInt("trans_cnt");
                String trans_type_cd = rs.getString ("trans_type_cd");
                String assoc_id = rs.getString("associate_id");
               
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, trans_date, trans_cnt, 
                                            "", assoc_id, trans_amt, null,
                                            trans_type_cd,  null);
                
                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " +ex.getMessage());
        }
        return transactions;
    }
    
    /*
     * 
     */
    public static TransHdrRecord findTransactionByHdrId (String hdrId) {
            
        TransHdrRecord record = null ;
        
        String sql = "select trans_hdr_id, customer_id, associate_id, "
                + "trans_type_cd, trans_amt,reference_txt, trans_cnt, "
                + "trans_dt, to_char(trans_dt, 'DD-MON-YYYY hh24:mi am') "
                + "as time from trans_hdr where trans_hdr_id = "+hdrId;
            
        try {
            Statement ss = DBUtils.getConnection().createStatement();
            
            ResultSet result = ss.executeQuery(sql);
            if(result.next()) {

                String trans_hdr_id = result.getString("trans_hdr_id");
                String customer_id = result.getString("customer_id");
                String associate_id = result.getString("associate_id");
                String trans_type_cd = result.getString("trans_type_cd");
                String trans_amt = result.getString("trans_amt");
                String reference_txt = result.getString("reference_txt");
                int count = result.getInt("trans_cnt");
                String date = result.getString("trans_dt");
                String time = result.getString("time");
            
                record = new TransHdrRecord (trans_hdr_id, date, count,
                            customer_id, associate_id, trans_amt, time,
                            trans_type_cd, reference_txt);
            }
                        
            result.close();
           
         } catch (java.sql.SQLException ex) {
            
            System.out.println("SQL Exception Occurred: " + ex.getMessage());
        }
        return record;
    }
    
     /*
     * 
     */
    public static ArrayList<TransHdrRecord>
            findTransactionsByMonthAndAssociate (String date, String assocId) {
            
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
                
        try {

            findTransactionsByMonthAndAssociate.setString(1,date);
            findTransactionsByMonthAndAssociate.setString(2,assocId);
            rs = findTransactionsByMonthAndAssociate.executeQuery();
           
            while (rs.next()) {

                String trans_hdr_id = rs.getString("trans_hdr_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_date = rs.getString("saledate");
                int trans_cnt = rs.getInt("trans_cnt");
                String trans_type_cd = rs.getString ("trans_type_cd");
                
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, trans_date, trans_cnt, 
                                            "", assocId, trans_amt, null,
                                            trans_type_cd,  null);
                
                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " +ex.getMessage());
        }
        return transactions;
    }
    
    /*
     * 
     */
    public static ArrayList<TransHdrRecord>
            findTransactionsByMemberIdPerMonth (String memberId, String date) {
       
        ArrayList<TransHdrRecord> transactions = new ArrayList<TransHdrRecord>();
                
        try {

            findTransactionsByMemberIdPerMonth.setString(1,date);
            findTransactionsByMemberIdPerMonth.setString(2,memberId);
            rs = findTransactionsByMemberIdPerMonth.executeQuery();
           
            while (rs.next()) {

                String trans_hdr_id = rs.getString("trans_hdr_id");
                String trans_amt = rs.getString("trans_amt");
                String trans_date = rs.getString("saledate");
                int trans_cnt = rs.getInt("trans_cnt");
               
                TransHdrRecord dti =
                        new TransHdrRecord (trans_hdr_id, trans_date, trans_cnt, 
                                            memberId, null, trans_amt, null, "", null);
                
                transactions.add(dti);
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " +ex.getMessage());
        }
        return transactions;
    }
    
    /*
     * 
     */
    public static String findTransactionDate (String saleHdrId) {
            
        String trans_dt = null;
                
        try {

            findTransactionDateBySaleHdrId.setString(1,saleHdrId);
            rs = findTransactionDateBySaleHdrId.executeQuery();
           
            if (rs.next()) {

                trans_dt = rs.getString("trans_dt");
            }
            
            rs.close();
            
        }  catch (java.sql.SQLException ex) {
            System.out.println("SQL Exception Occurred: " +ex.getMessage());
        }
        return trans_dt;
    }
}
