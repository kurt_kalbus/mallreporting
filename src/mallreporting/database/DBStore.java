package mallreporting.database;

import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author kkalb
 */
public class DBStore {

    public static String getStoreId() {
        
        String query = "select store_id from store";
        String store_id = "";
        
        try {
            Statement ss = DBUtils.getConnection().createStatement();
            
            ResultSet result = ss.executeQuery(query);
            if(result.next()) {

                store_id = result.getString(1);
            }
            result.close();
            ss.close();
           
        } catch (java.sql.SQLException ex) {
            
            System.out.println("getStoreId Failed to find store_id! Please see console log. "+ex.toString());
        }
         
         return store_id;
    }
}
