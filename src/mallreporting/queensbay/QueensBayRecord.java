/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mallreporting.queensbay;

import java.math.BigDecimal;

/**
 *
 * @author kkalb
 */
public class QueensBayRecord {
  
    private String hour = "";
    private int num_trans = 0;
    private BigDecimal total_sales = new BigDecimal ("0.00", QueensBay.mc);
    private BigDecimal total_void = new BigDecimal ("0.00", QueensBay.mc);
    private BigDecimal gst = new BigDecimal ("0.00", QueensBay.mc);
    
    public QueensBayRecord (String hour) {
   
        this.hour = hour;
    }
    
    public void addVoid (BigDecimal amount) {
        
        total_void = total_void.add(amount);
        num_trans++;
    }
    
    public void addItem (BigDecimal amount) {
        
        total_sales = total_sales.add(amount);
        num_trans++;
    }
    
    public String get_hour() {
        return hour;
    }
    
    public BigDecimal get_total_sales() {
        
        return total_sales;
    }
    
    public int get_num_trans() {
        
        return num_trans;
    }
}
