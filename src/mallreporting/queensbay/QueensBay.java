package mallreporting.queensbay;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import mallreporting.Base64New;
import mallreporting.TenantData;
import mallreporting.TransHdrRecord;
import mallreporting.WriteReportToFile;
import mallreporting.database.DBStore;
import mallreporting.database.DBTransHdr;
import mallreporting.database.DBUtils;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author kkalb
 */
public class QueensBay {
    
    public static String versionNumber = ".001";
    public static String appName = "queensbay";
    private static TenantData tenantData;
    private static String date = "";
    public static MathContext mc = new MathContext(10,RoundingMode.HALF_UP);
    public static String ftp_host = "";
    public static String ftp_user = "";
    public static String ftp_enc_password = "";
    private static ArrayList<QueensBayRecord> records = new ArrayList<QueensBayRecord>();
    
    public QueensBay (TenantData tenantData) {
        
        this.tenantData = tenantData;
    }
    
    public void runIt() {
       
        try {
            
            DateFormat dateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
            Calendar today = Calendar.getInstance();
            date = dateFormat1.format(today.getTime());
               
            DBUtils.initConnection();  
            DBUtils.initStatements();
           
            ftp_host = tenantData.get_ftp_host();
            ftp_enc_password = tenantData.get_ftp_enc_password();
            ftp_user = tenantData.get_ftp_user();
            
            if (tenantData == null) {
                
                System.out.println ("Tenant data NOT FOUND!!");
                System.exit(1);
            }
            
            ArrayList<TransHdrRecord> transactions =
                    DBTransHdr.findTransactionsByDateOrderByTime (date);
             
            for (int i=0; i<10; i++) {
                 
                String hour = "0" + String.valueOf(i);
                QueensBayRecord record = new QueensBayRecord(hour);  
                records.add(record);
            }
             
            for (int i=10; i<24; i++) {
                 
                String hour = String.valueOf(i);  
                QueensBayRecord record =  new QueensBayRecord(hour); 
                records.add(record);
            }
             
            for (int i=0; i<transactions.size(); i++) {
                 
                TransHdrRecord trans = transactions.get(i);
                int hour = Integer.valueOf(trans.get_trans_time().substring(0,2));
                
                String transType = trans.get_trans_type_cd();
                QueensBayRecord record = records.get(hour);
                
                if (transType.equals ("V")) {
                    record.addVoid(trans.get_trans_amt());
                } else {
                    record.addItem(trans.get_trans_amt());
                }
            }
            
            DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
            String timeStamp = dateFormat.format(today.getTime());
            String txnYear = (new SimpleDateFormat("yyyy")).format(today.getTime());
            String txnMonth = (new SimpleDateFormat("MM")).format(today.getTime());
            String txnDay = (new SimpleDateFormat("dd")).format(today.getTime());
           
            String storeName = DBStore.getStoreId();
            String filenameBase  = storeName +"_"+timeStamp+"_H_";
            String delim = ",";
             
            String filename = "";
          
            ArrayList<String> reportLines = new ArrayList<String>();
             
            FTPClient ftp = new FTPClient();
            ftp.connect(ftp_host);
           
            String dec_password = new String(Base64New.decode(ftp_enc_password));
            
            if (ftp.login(ftp_user, dec_password)) {
                System.out.println ("FTP Connected!");
            } else {
                System.out.println ("FTP Failed!");
                System.exit(ftp.getReplyCode());
            }
           
            System.out.println ("reply code = "+ftp.getReplyCode());
            System.out.println ("reply string = "+ftp.getReplyString());
           
            for (QueensBayRecord record: records) {
                 
                BigDecimal totalSales = record.get_total_sales();
                
                if (totalSales.compareTo(BigDecimal.ZERO) == 0) {
                    continue;
                }
                
                reportLines.clear();
                reportLines.add("EID,TxnYear,TxnMonth,TxnDate,TxnHour,TxnAmount,TxnVoid");
                
                String line = storeName +delim+ txnYear +delim+ txnMonth +delim+ 
                              txnDay +delim+ record.get_hour() +delim+ 
                              record.get_total_sales() + delim + "0.00";
                        
                reportLines.add(line);
                
                reportLines.add("END");
                filename = filenameBase + record.get_hour() + ".csv";
                WriteReportToFile.write (reportLines, null, filename);
                
                BufferedInputStream bos = new BufferedInputStream(new FileInputStream (filename));
                
                if (ftp.storeFile(filename, bos)) {

                    System.out.println ("ftp worked");
                } else {
                    System.out.println ("ftp sucked");
                    System.exit(ftp.getReplyCode());
                }

                System.out.println ("reply code = "+ftp.getReplyCode());
                System.out.println ("reply string = "+ftp.getReplyString());
                bos.close();
            }
            
            int retCode = ftp.getReplyCode();       
            ftp.disconnect();
          
        } catch (Exception ex) {
            
            System.out.println (ex.toString());
        }
    }
    
     public static void main() {
        
        QueensBay queensBay = new QueensBay(null);
        queensBay.runIt();
    }
}
