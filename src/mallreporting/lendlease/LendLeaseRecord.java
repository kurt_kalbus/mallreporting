/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mallreporting.lendlease;

import java.math.BigDecimal;

/**
 *
 * @author kkalb
 */
public class LendLeaseRecord {
  
    private String hour = "";
    private int num_trans = 0;
    private BigDecimal total_sales = new BigDecimal ("0.00", LendLease.mc);
  
    public LendLeaseRecord (String hour) {
    
        this.hour = hour;
    }
    
    public void addItem (BigDecimal amount) {
        
        total_sales = total_sales.add(amount);
        num_trans++;
    }
    
    public String get_hour() {
        return hour;
    }
    
    public BigDecimal get_total_sales() {
        
        return total_sales;
    }
    
    public int get_num_trans() {
        
        return num_trans;
    }
}
