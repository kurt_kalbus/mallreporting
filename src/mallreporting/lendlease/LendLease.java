package mallreporting.lendlease;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import mallreporting.Base64New;
import mallreporting.TenantData;
import mallreporting.TransHdrRecord;
import mallreporting.WriteReportToFile;
import mallreporting.database.DBTransHdr;
import mallreporting.database.DBUtils;
import org.apache.commons.net.ftp.FTPClient;

/**
 *
 * @author kkalb
 */
public class LendLease {
   
    private static String appName = "lendlease";
    private static TenantData tenantData;
    private static String date = "";
    public static MathContext mc = new MathContext(10,RoundingMode.HALF_UP);
    private static String ftp_host = "";
    private static String ftp_user = "";
    private static String ftp_enc_password = "";
    private static ArrayList<LendLeaseRecord> records = new ArrayList<LendLeaseRecord>();
   
    public LendLease (TenantData tenantData) {
        
        this.tenantData = tenantData; 
    }
    
    public void runIt() {
       
        try {
            
            DateFormat dateFormat1 = new SimpleDateFormat("dd-MMM-yyyy");
            Calendar today = Calendar.getInstance();
            date = dateFormat1.format(today.getTime());
               
            DBUtils.initConnection();  
            DBUtils.initStatements();
       
            ftp_host = tenantData.get_ftp_host();
            ftp_enc_password = tenantData.get_ftp_enc_password();
            ftp_user = tenantData.get_ftp_user();
            
            if (tenantData == null) {
                
                System.out.println ("Tenant data NOT FOUND!!");
                System.exit(1);
            }
            
            ArrayList<TransHdrRecord> transactions =
                    DBTransHdr.findTransactionsByDateOrderByTime (date);
             
            for (int i=0; i<10; i++) {
                 
                String hour = "0" + String.valueOf(i) + "59"; 
                LendLeaseRecord record = new LendLeaseRecord(hour);
                records.add(record);
            }
             
            for (int i=10; i<24; i++) {
                 
                String hour = String.valueOf(i) + "59";
                 
                LendLeaseRecord record =  new LendLeaseRecord(hour);
                             
                records.add(record);
            }
             
            for (int i=0; i<transactions.size(); i++) {
                 
                TransHdrRecord trans = transactions.get(i);
                int hour = Integer.valueOf(trans.get_trans_time().substring(0,2));
                
                LendLeaseRecord record = records.get(hour);
                record.addItem(trans.get_trans_amt());    
            }
            
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd_HHmmss");
            String timeStamp = dateFormat.format(today.getTime());
           
            String filename  = tenantData.get_store_id() +"_"+timeStamp+".txt";
            String delim = "|";
             
            SimpleDateFormat formatOld = new SimpleDateFormat("dd-MMM-yyyy");
            Date reportDateOld = formatOld.parse(date);
             
            SimpleDateFormat formatNew = new SimpleDateFormat("ddMMyyyy");
            String reportDateString = formatNew.format(reportDateOld);
            ArrayList<String> reportLines = new ArrayList<String>();
             
            for (LendLeaseRecord record: records) {
                 
                String line = tenantData.get_store_id() + delim +
                        reportDateString + delim +
                        tenantData.get_batch_id() + delim +
                        record.get_hour() + delim +
                        record.get_num_trans() + delim +
                        record.get_total_sales() + delim +
                        "0.00" + delim +
                        "0.00" + delim +
                        "0";
                
                reportLines.add(line);
            }
                        
            WriteReportToFile.write (reportLines, null, filename);
            
            FTPClient ftp = new FTPClient();
            ftp.connect(ftp_host);
           
            String dec_password = new String(Base64New.decode(ftp_enc_password));
            
            if (ftp.login(ftp_user, dec_password)) {
                System.out.println ("FTP Connected!");
            } else {
                System.out.println ("FTP Failed!");
                System.exit(ftp.getReplyCode());
            }
           
            System.out.println ("reply code = "+ftp.getReplyCode());
            System.out.println ("reply string = "+ftp.getReplyString());
           
            BufferedInputStream bos = new BufferedInputStream(new FileInputStream (filename));
            if (ftp.storeFile(filename, bos)) {
                
                System.out.println ("ftp worked");
            } else {
                System.out.println ("ftp sucked");
                System.exit(ftp.getReplyCode());
            }
            
            int retCode = ftp.getReplyCode();
            System.out.println ("reply code = "+retCode);
            System.out.println ("reply string = "+ftp.getReplyString());
            bos.close();
            
            ftp.disconnect();
           
        } catch (Exception ex) {
            
            System.out.println (ex.toString());
        }
    }
    
    public static void main() {
        
        LendLease lendLease = new LendLease(null);
        lendLease.runIt();
    }
}
